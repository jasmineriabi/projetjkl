package com.example.news.data

import com.example.news.data.remote.NewsRemoteDataSource

class NewsRepository(private val newsDataSource: NewsRemoteDataSource) {
    suspend fun getNews() = newsDataSource.getNews()
}
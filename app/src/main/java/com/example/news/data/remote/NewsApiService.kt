package com.example.news.data.remote

import com.example.news.data.News
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApiService {
    @GET("/v2/top-headlines")
    suspend fun getAllNews(
        @Query("country") iso639: String = "fr",
        @Query("apiKey") apiKey: String  = "4ab9030d0ffd41cf9c4c411fa39e7606"
    ): News
}
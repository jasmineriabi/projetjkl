package com.example.news

import android.app.Application
import com.example.news.data.NewsRepository
import com.example.news.data.remote.NewsRemoteDataSource

class App : Application(){
    companion object {
        lateinit var instance: App

        val newsRepository by lazy {
            NewsRepository(NewsRemoteDataSource)
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}

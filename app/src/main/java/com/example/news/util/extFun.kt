package com.example.news.util

import androidx.fragment.app.Fragment
import com.example.news.App

fun Fragment.getViewModelFactory(): ViewModelFactory {
    val repository = App.newsRepository
    return ViewModelFactory(repository, this)
}
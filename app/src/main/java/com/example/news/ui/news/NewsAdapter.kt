package com.example.news.ui.news

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.news.R
import com.example.news.data.Article
import com.example.news.databinding.ItemNewsBinding
import com.squareup.picasso.Picasso


class NewsAdapter() : ListAdapter<Article, NewsAdapter.ViewHolder>(NewsDiffCallback()) {

    class ViewHolder(val binding: ItemNewsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindNews(magic: Article) {
            binding.title.text = magic.title

            Picasso.get()
                .load(magic.url)
                .error(R.drawable.ic_baseline_newspaper_24)
                .placeholder(R.drawable.ic_baseline_newspaper_24)
                .into(binding.img)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemNewsBinding.inflate(LayoutInflater.from(parent.context))
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindNews(getItem(position))
    }

}

class NewsDiffCallback : DiffUtil.ItemCallback<Article>() {
    override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
        return oldItem.title == newItem.title
    }

    override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
        return oldItem == newItem
    }
}